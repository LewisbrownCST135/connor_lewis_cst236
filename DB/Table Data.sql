
LOCK TABLES ORDERS WRITE;
/*!40000 ALTER TABLE ORDERS DISABLE KEYS */;
INSERT INTO ORDERS VALUES (3,2,'2017-12-09 03:36:56','Sega Genesis',99.99,'Admin, General','clow2@my.gcu.edu',1),(4,5,'2017-12-09 03:36:59','Game Boy Color',115,'Admin, General','clow2@my.gcu.edu',1),(5,3,'2017-12-11 23:40:08','Atari 2600',199,'Admin, General','clow2@my.gcu.edu',4),(6,3,'2017-12-11 23:40:08','Atari 2600',199,'Admin, General','clow2@my.gcu.edu',4),(7,9,'2017-12-11 23:40:08','Magnavox Odyssey',200,'Admin, General','clow2@my.gcu.edu',1),(8,3,'2017-12-12 01:11:57','Atari 2600',199,'Admin, General','clow2@my.gcu.edu',1),(9,5,'2017-12-12 01:12:24','Game Boy Color',115,'Admin, General','clow2@my.gcu.edu',1),(10,9,'2017-12-11 00:00:00','Magnavox Odyssey',200,'ADMIN','clow2@my.gcu.edu',2),(11,9,'2017-12-11 00:00:00','Magnavox Odyssey',200,'ADMIN','clow2@my.gcu.edu',2),(12,9,'2017-12-11 00:00:00','Magnavox Odyssey',200,'ADMIN','clow2@my.gcu.edu',2),(13,2,'2017-12-12 01:17:45','Sega Genesis',99.99,'ADMIN','clow2@my.gcu.edu',3),(14,2,'2017-12-12 01:17:49','Sega Genesis',99.99,'ADMIN','clow2@my.gcu.edu',3),(15,1,'2017-12-12 01:19:33','SNES',199.99,'Admin, General','clow2@my.gcu.edu',1),(16,2,'2017-12-12 01:19:33','Sega Genesis',99.99,'Admin, General','clow2@my.gcu.edu',1),(17,3,'2017-12-12 01:19:33','Atari 2600',199,'Admin, General','clow2@my.gcu.edu',1),(18,4,'2017-12-12 01:19:33','ZX Spectrum',89,'Admin, General','clow2@my.gcu.edu',1),(19,5,'2017-12-12 01:19:33','Game Boy Color',115,'Admin, General','clow2@my.gcu.edu',1),(20,6,'2017-12-12 01:19:33','ColecoVision',205,'Admin, General','clow2@my.gcu.edu',1),(21,7,'2017-12-12 01:19:33','Commodore 64',335,'Admin, General','clow2@my.gcu.edu',1),(22,8,'2017-12-12 01:19:33','Amstrad CPC',123,'Admin, General','clow2@my.gcu.edu',1),(23,8,'2017-12-12 01:19:33','Amstrad CPC',123,'Admin, General','clow2@my.gcu.edu',1),(24,10,'2017-12-12 01:19:33','Intellivision',299,'Admin, General','clow2@my.gcu.edu',1),(25,11,'2017-12-11 03:28:07','Another PRODUCT',45.99,'Admin, General','clow2@my.gcu.edu',1),(26,4,'2017-12-11 03:28:07','ZX Spectrum',89,'Admin, General','clow2@my.gcu.edu',1),(27,9,'2017-12-11 09:08:36','Magnavox Odyssey',200,'Cena, John','john@cena.nosee',1),(28,1,'2017-12-12 17:43:29','SNES',199.99,'Wilkerson, Robert','robby.bobby@email.net',1),(29,1,'2017-12-12 17:43:29','SNES',199.99,'Wilkerson, Robert','robby.bobby@email.net',1),(30,11,'2017-12-12 17:50:45','Another PRODUCT',45.99,'Cankatoo, Allie','theRealAC@email.net',1),(31,1,'2017-12-12 17:50:45','SNES',199.99,'Cankatoo, Allie','theRealAC@email.net',1),(32,1,'2017-12-12 17:51:19','SNES',199.99,'Cankatoo, Allie','theRealAC@email.net',1),(33,1,'2017-12-13 08:28:43','SNES',199.99,'Kart, Hevin','almost.famous@email.net',1),(34,2,'2017-12-13 08:28:43','Sega Genesis',99.99,'Kart, Hevin','almost.famous@email.net',1),(35,1,'2017-12-13 08:45:02','SNES',199.99,'Chef, Jeff','Mynameis@jeff.com',1),(36,1,'2017-12-13 08:45:02','SNES',199.99,'Chef, Jeff','Mynameis@jeff.com',1),(37,6,'2017-12-13 08:45:02','ColecoVision',205,'Chef, Jeff','Mynameis@jeff.com',1);
/*!40000 ALTER TABLE ORDERS ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES PRODUCT WRITE;
/*!40000 ALTER TABLE PRODUCT DISABLE KEYS */;
INSERT INTO PRODUCT VALUES (1,'SNES',199.99,'super nintendo entertainment system','SNES.png'),(2,'Sega Genesis',99.99,'the genesis of sega','SegaGenesis.png'),(3,'Atari 2600',199,'the best atari console','Atari2600.png'),(4,'ZX Spectrum',89,'never had an american release','zxspectrum.png'),(5,'Game Boy Color',115,'the second best gameboy','GameBoyColor.png'),(6,'ColecoVision',205,'does anyone remember Coleco?','ColecoVision.png'),(7,'Commodore 64',335,'th commodore 64 commoders 64','Commodore64.png'),(8,'Amstrad CPC',123,'the amstrad amstrads','AmstradCPC.png'),(9,'Magnavox Odyssey',200,'the odyssey of the Magnavox','MagnavoxOdyssey.png'),(10,'Intellivision',299,'the Intellivision isnt so intelligent','Intellivision.png'),(11,'Another PRODUCT',45.99,'Additional test PRODUCT for additional testing. It has a longer-than average description. It is a reminder that the add PRODUCT form should have a larger input for the description part. Features include: the sweet image that definitely will work I hope; a low price tag; nostalgia. Order now!','SNES.png');
/*!40000 ALTER TABLE PRODUCT ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table USER
--

LOCK TABLES USER WRITE;
/*!40000 ALTER TABLE USER DISABLE KEYS */;
INSERT INTO USER VALUES (1,'ADMIN','CST-236','General','Admin','clow2@my.gcu.edu','1234567890121416','2017-12-07',1,'Connor Fake Low','280 Dandy Lane Westchester, DB 19P1G','456'),(2,'Thanks','giving','Connor','Low','connorjameslow@gmail.com','0000111122223333','0000-00-00',0,'Connor Low',NULL,'444'),(3,'john','cena','John','Cena','john@cena.nosee','0000111122229203','2020-01-01',0,'John Cena','Cool street','543'),(4,'bobby','qpzm','Robert','Wilkerson','robby.bobby@email.net','0000111122223343','2020-02-20',0,'Robert J Wilkerson','1800 five eight eight','123'),(5,'alli3','qpzm','Allie','Cankatoo','theRealAC@email.net','0000141122223333','2022-12-12',0,'My Mother','','123'),(6,'hev','qpzm','Hevin','Kart','almost.famous@email.net','0070111122223333','2020-01-01',0,'Hevin F Kart','Homeslice Ln 98023 MN','123'),(7,'MyNameJeff','password','Jeff','Chef','Mynameis@jeff.com','0000111122223333','2006-06-06',0,'Jeff J. Jeff','Billing Address','235');
/*!40000 ALTER TABLE USER ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-14 19:09:33

