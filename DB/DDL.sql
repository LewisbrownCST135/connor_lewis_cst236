-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: retro
-- ------------------------------------------------------
-- Server version	5.6.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table CART
--

DROP TABLE IF EXISTS CART;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE CART (
  PRODUCT_ID int(11) NOT NULL,
  USER_ID int(11) NOT NULL,
  KEY PRODUCT_ID (PRODUCT_ID),
  KEY USER_ID (USER_ID),
  CONSTRAINT CART_ibfk_1 FOREIGN KEY (PRODUCT_ID) REFERENCES PRODUCT (ID),
  CONSTRAINT CART_ibfk_2 FOREIGN KEY (USER_ID) REFERENCES USER (ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table catalog
--

DROP TABLE IF EXISTS catalog;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Table structure for table ORDERS
--

DROP TABLE IF EXISTS ORDERS;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE ORDERS (
  ID int(11) NOT NULL AUTO_INCREMENT,
  PRODUCT_ID int(11) NOT NULL,
  ORDER_DATE datetime DEFAULT CURRENT_TIMESTAMP,
  PRODUCT varchar(100) NOT NULL,
  PRICE double NOT NULL,
  USER varchar(30) DEFAULT NULL,
  CONTACT varchar(152) DEFAULT NULL,
  QUANTITY int(11) DEFAULT NULL,
  PRIMARY KEY (ID),
  UNIQUE KEY ID (ID),
  KEY idx_ORDERS_PRODUCT_ID (PRODUCT_ID),
  KEY idx_ORDERS_QUANTITY (QUANTITY),
  KEY idx_ORDERS_ORDER_DATE (ORDER_DATE)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table PRODUCT
--

DROP TABLE IF EXISTS PRODUCT;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE PRODUCT (
  ID int(11) NOT NULL AUTO_INCREMENT,
  NAME varchar(100) NOT NULL,
  PRICE double NOT NULL,
  DESCRIPTION text NOT NULL,
  IMAGE varchar(255) NOT NULL,
  PRIMARY KEY (ID),
  KEY idx_PRODUCT_PRICE (PRICE)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table USER
--

DROP TABLE IF EXISTS USER;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE USER (
  ID int(11) NOT NULL AUTO_INCREMENT,
  USERNAME varchar(30) NOT NULL,
  PASSWORD varchar(50) NOT NULL,
  FIRST_NAME varchar(30) NOT NULL,
  LAST_NAME varchar(30) NOT NULL,
  EMAIL varchar(50) NOT NULL,
  CREDIT_CARD varchar(16) DEFAULT '',
  CARD_DATE date DEFAULT NULL,
  ADMIN tinyint(1) DEFAULT '0',
  CARD_HOLDER varchar(100) DEFAULT NULL,
  BILLING_ADDRESS varchar(100) DEFAULT NULL,
  CARD_CVC varchar(100) DEFAULT NULL,
  PRIMARY KEY (ID),
  KEY idx_USER_USERNAME (USERNAME),
  KEY idx_USER_CREDIT_CARD (CREDIT_CARD)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-14 19:09:33
