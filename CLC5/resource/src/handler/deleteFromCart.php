<?php
/*
Project: CST-236 CLC4 4.3
File: deleteFromCart.php 1.0
Authors: Connor Low
Date: 10/22/17
Synopsis: updates local cart and DB.
*/

// update DB
$cartService = new CartBusinessService($user);
$cartService->removeProduct(new Product($_GET["productId"]));

// update local cart
$_SESSION["cart"]->setProductArray($cartService->getProducts());
