<?php
/*
Project: CST-236 CLC4 3.1
File: Delete Product.php 1.0
Authors: Connor Low
Date: 10/8/17
Synopsis: Handler for updating products
reference: func/connect.php
*/

$ini = parse_ini_file("../resource/config.ini", true);
include_once $ini["Path"]["header"];

$product = new Product($_POST["id"], $_POST["name"], $_POST["price"], $_POST["description"], $_POST["image"]);
$service = new ProductDataService($product);
$result = $service->update();

include "AdminTools.php";

?>
