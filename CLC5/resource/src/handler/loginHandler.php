<?php
/*
Project: CST-236 CLC5 5.1
File: loginHandler.php 4.1
Authors: Connor Low, Lewis Brown
Date: 11/2/17
Synopsis: Used to check credentials against DB during a login.
 Also creates and stores a Users object in SESSION
Update: LoginDS is now encapsulated in the LoginBR class
*/

$ini = parse_ini_file("../resource/config.ini", TRUE);
include_once $ini["Path"]["autoloader"];
session_start();

// get posted variables
$password = $_POST["password"];
$username = $_POST["username"];

// Business Rules
// create a new business service using the fields username and password
$service = new UserBusinessService(new User(-1, $username, $password));

// use loginUser to either get user information or return false if login credentials are incorrect
if ($result = $service->loginUser()) {
    $row = $result->fetch_assoc();
    $user = new User($row["ID"], $row["USERNAME"], $row["PASSWORD"], $row["FIRST_NAME"],
        $row["LAST_NAME"], $row["EMAIL"], $row["ADMIN"], $row["CREDIT_CARD"],
        $row["CARD_CVC"], $row["CARD_DATE"], $row["CARD_HOLDER"], $row["BILLING_ADDRESS"]);

    // store user in session
    $_SESSION["user"] = $user;

    // establish cart and store in session
    $cartService = new CartBusinessService($user);
    if ($cartService->getProducts()) {
        $cart = new Cart($cartService->getProducts());
        $_SESSION["cart"] = $cart;
    }

    include "Home.php";
} else {
    $error = 2;
    include "LoginForm.php";
}
?>