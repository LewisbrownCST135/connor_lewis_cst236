<?php
/*
Project: CST-236 CLC4 3.1
File: Delete Product.php 1.0
Authors: Connor Low
Date: 10/8/17
Synopsis: Handler for finding products
reference: func/connect.php
*/

$ini = parse_ini_file("../resource/config.ini", true);
include_once $ini["Path"]["header"];
?>
<html>
<?php include $ini["Template"]["head"]; ?>
<body>
<?php include $ini["Template"]["nav"]; ?>
<div id="main_content">
    <div class="CenterContent">
        <div class="AdminSearch">
            <div class="Container">
                <h3>Results: </h3>
                <a href="AdminTools.php?find">back</a>
                <?php
                $id = $_POST["id"];
                $name = $_POST["name"];
                $description = $_POST["description"];
                $price = $_POST["price"];
                $product = new Product($id, $name, $price, $description);
                $service = new ProductDataService($product);
                $result = $service->read(TRUE);
                foreach ($result as $row) {
                    ?>

                    <div class="Result">
                        <a <?php echo "href='AdminTools.php?action=edit&id=" . $row['ID'] . "' " ?>>
                                ID[<?php echo $row["ID"] ?>] <?php echo $row["NAME"] ?>
                        </a>
                        <div class="QuickEdit"></div>
                    </div>

                    <?php
                }
                ?>
                <a href="AdminTools.php?find">back</a>
            </div>
        </div>
    </div>
</div>
<?php include $ini["Template"]["foot"]; ?>
</body>
</html>
