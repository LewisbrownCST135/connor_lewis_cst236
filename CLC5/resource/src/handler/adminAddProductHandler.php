<?php
/**
 * Created by PhpStorm.
 * User: lewis brown
 * Date: 11/5/2017
 * Time: 2:37 PM
 */
$ini = parse_ini_file("../resource/config.ini", true);
include_once $ini["Path"]["header"];
include "AdminTools.php";

    $name = $_POST["name"];
    $image = $_POST["image"];
    $description = $_POST["description"];
    $price = $_POST["price"];
    $product = new Product(null, $name, $price, $description, $image);
    $service = new ProductDataService($product);
    if($name != "" && $description != "" && $price != null) {
        $result = $service->create();
        echo "<p align='center'>Product added</p>";
    }else {
        echo "<p align='center'>you are missing something</p>";
    }



