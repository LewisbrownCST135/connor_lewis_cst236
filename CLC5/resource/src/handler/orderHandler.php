<?php
/*
Project: CST-236 CLC4 4.3
File: orderHandler 1.0
Authors: Connor Low
Date: 10/22/17
Synopsis: Used to complete an order action
*/
$ini = parse_ini_file("../resource/config.ini", TRUE);
include_once $ini["Path"]["header"];

// get posted variables
$number = $_POST["card"];
$date = $_POST["date"];
$cvc = $_POST["cvc"];
$holder = $_POST["holder"];
$billing = $_POST["billing"];
$address = $_POST["shipping1"] . " " . $_POST["shipping2"];
/**
 * @var $user User
 * @var $cart Cart
 * @var $product Product
 */
$user->setCardN($number);
$user->setCardED($date);
$user->setCardH($holder);
$user->setCardCVC($cvc);
$user->setBillingAddress($billing);
$user->setAddress($address);

// update User
$userService = new UserBusinessService($user);
$userService->updateUser();

// Business Rules - Validate Card
$service = new OrderBusinessService();

if ($service->invalidOrder($user)) {

    // if errors, go back and print error
    $error = 1;
    include "PlaceOrder.php";
} else {

    // get cart from DB
    $cartService = new CartBusinessService($user);

    // make a payment
    $paymentService = new PaymentService($user, $_SESSION["TC"]);
    $_SESSION["lastOrder"] = array();
    $i = 0;
    if ($paymentService->authorize()) {
        if ($paymentService->order())
            if ($receipt = $paymentService->receipt()) {
                $orderService = new OrderBusinessService();

                if ($orderService->createOrder($cartService->getProducts(), $_SESSION["user"])) {
                    $cartService->emptyCart();

                    // navigate to confirmation
                    include "OrderConfrimation.php";

                    // update cart
                    $_SESSION["TC"] = 0;
                } else {
                    $error = 1;
                    include "PlaceOrder.php";
                }
            } else {
                $error = 1;
                include "PlaceOrder.php";
            }
        else {
            $error = 1;
            include "PlaceOrder.php";
        }
    } else {
        $error = 1;
        include "PlaceOrder.php";
    }


}

?>