<?php
/**
 * Authors: Connor Low, Lewis Brown
 * Project Version: Milestone 5.1
 * File Version: registerHandler 4.0
 * Date: 11/2/17
 * Synopsis: Handles Register service
 */
$ini = parse_ini_file("../resource/config.ini", TRUE);
include_once $ini["Path"]["autoloader"];
session_start();

// get posted variables
// getting the variables from the config.ini or the User Table
$firstName = $_POST["firstName"];
$lastName = $_POST["lastName"];
$username = $_POST["username"];
$password = $_POST["password"];
$email = $_POST["email"];
$cardNum = $_POST["cc1"] . $_POST["cc2"] . $_POST["cc3"] . $_POST["cc4"];
$cvc = $_POST["cvc"];
$cardDate = $_POST["expiration"];
$cardHolder = $_POST["holder"];

// create registration object
$user = new User(null, $username, $password, $firstName, $lastName, $email, FALSE, $cardNum, $cvc, $cardDate, $cardHolder);
$service = new UserBusinessService($user);

// Attempt to create and login new user
if ($service->checkForUsername()) { // see if username is taken
    $error = 2;
    include "RegisterForm.php";
} else if ($service->registerUser()) {// check length rules and attempt to insert
    include "loginHandler.php";
} else {
    $error = 1;
    include "RegisterForm.php"; // display errors on RegisterForm
}

/*
$rules = new RegisterBR($user);

// check if variables match requirements
//takes breaks rules from Register_BR
if ($rules->breaksRules()) {
    $error = 1;
    include "RegisterForm.php";
} else if ($service->usernameTaken()) {
    $error = 2;
    include "RegisterForm.php";
} else {
    $rules->validate();
    include "loginHandler.php";
}*/