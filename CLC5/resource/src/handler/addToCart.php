<?php
/*
Project: CST-236 CLC4 5.1
File: addToCart.php 2.0
Authors: Connor Low
Date: 11/2/17
Synopsis: updates local cart and DB.
*/

$ini = parse_ini_file("../resource/config.ini", TRUE);
session_start();

// get information for product to add from DB
$productID = $_GET["productId"];
$productService = new ProductBusinessService(new Product($productID));

// create product with query results
$row = $productService->selectProduct()->fetch_assoc();
$addedProduct = new Product($row["ID"], $row["NAME"], $row["PRICE"], $row["DESCRIPTION"], $row["IMAGE"]);

// update cart in DB
$cartService = new CartBusinessService($user); // $user is defined in CartView/header
$cartService->addProduct($addedProduct);

// create cart from DB. Initialize if necessary
if (is_null($_SESSION["cart"]) || !($_SESSION["cart"] instanceof Cart))
    $_SESSION["cart"] = new Cart($cartService->getProducts());
else
    $_SESSION["cart"]->setProductArray($cartService->getProducts());


