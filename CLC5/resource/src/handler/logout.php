<?php
/*
Project: CST-236 CLC4 5.1
File: logout 3.0
Authors: Connor Low
Date: 10/26/17
Synopsis: Log out a user
*/
session_start();
session_destroy();
include "LoginForm.php";

?>