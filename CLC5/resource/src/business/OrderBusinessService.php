<?php
/*
Project: CST-236 CLC5 7.1
File: OrderBusinessService 2.2. Previously: RecordBusinessService
Authors: Connor Low, Lewis Brown
Date: 12/11/17
Synopsis: Used to manage Orders
 */

class OrderBusinessService
{
    private $order;

    /**
     * RecordBusinessService constructor.
     * @param $order
     */
    public function __construct(Order $order = null)
    {
        $this->order = $order;
    }

    /**
     * @param $number
     * @param $cv
     * @param $holder
     * @param $date
     * @return bool
     */
    public function invalidOrder(User $user)
    {
        // check if fields are blank
        return
            strlen($user->getCardN()) != 16 ||
            trim($user->getCardED()) == "" ||
            strlen($user->getCardCVC()) != 3 ||
            trim($user->getCardH()) == "" ||
            trim($user->getBillingAddress()) == "" ||
            trim($user->getAddress()) == "";
    }

    /**
     * @param bool $distinct
     * @return mixed
     */
    public function getCount($distinct = false)
    {
        $service = new OrderDataService();
        return $service->getCount($distinct);
    }

    /**
     * @param int $page
     * @return array|int
     */
    public function getPrimaryReport($page = 0)
    {
        // create safe pseudo Order
        $order = new Order();
        $order->setUser("");
        $order->setProduct("");
        $service = new OrderDataService($order);

        $reportSet = array(); // should contain orders
        $raw = $service->readPrimaryReport();
        $i = 0;
        /**
         * @var $o Order
         */
        foreach ($raw as $row) {

            // quantity check
            $exists = false;
            foreach ($reportSet as &$o) {
                if ($row["PRODUCT_ID"] == $o->getPId()) {
                    $o->setQuantity($o->getQuantity() + $row["QUANTITY"]);
                    $exists = true;
                }
            }

            // add to orders
            if (!$exists) {
                $nextOrder = new Order();
                $nextOrder->setProduct($row["PRODUCT"]);
                $nextOrder->setPId($row["PRODUCT_ID"]);
                $nextOrder->setQuantity($row["QUANTITY"]);
                $reportSet[$i++] = $nextOrder;
            }
        }

        // sort orders
        /**
         * @var $order Order
         * @var $result [] Order
         */
        $result = array();
        $rc = 0;
        foreach ($reportSet as &$order) {
            for ($i = 0; $i < count($result); $i++) {
                if ($order->getQuantity() > $result[$i]->getQuantity()) {
                    $temp = $order;
                    $order = $result[$i];
                    $result[$i] = $temp;
                }
            }
            $result[$rc++] = $order;
        }

        // select page of 10
        $truncatedResult = array();
        for ($i = 0; $i < 10; $i++) {
            $truncatedResult[$i] = $result[$i + ($page * 10)];
        }
        if (count($truncatedResult) < 1)
            return -1;
        return $truncatedResult;
    }

    /**
     * @param $productId
     * @return bool|mysqli_result
     */
    public function getProductReport($productId, $page = 0, $count = false)
    {

        $pseudoOrder = new Order();
        $pseudoOrder->setPId($productId);
        $service = new OrderDataService($pseudoOrder);
        $report = $service->readProductReport($page, $count);
        if ($report->num_rows > 0)
            return $report;
        else
            return FALSE;
    }

    /**
     * Creates a record in the DB
     *
     * @param $array array
     * @return bool
     */
    public function createOrder($array, $user)
    {
        // create orders
        $i = 0;
        $oNo = 0;
        $orders = array();
        /**
         * @var $p Product
         * @var $o Order
         * @var $user User
         */
        foreach ($array as $p) {
            $_SESSION["lastOrder"][$i++] = $p;

            // quantity check
            $exists = false;
            foreach ($orders as &$o) {
                if ($p->getName() == $o->getProduct()) {
                    $o->setQuantity($o->getQuantity() + 1);
                    $exists = true;
                }
            }

            // add to orders
            if (!$exists) {
                $nextOrder = new Order(-1, $user->getLastName() . ", " . $user->getFirstName(),
                    -1, $p->getId(), $p->getName(), $p->getPrice(), $user->getEmail());
                $orders[$oNo++] = $nextOrder;
            }
        }
        foreach ($orders as $o) {
            $service = new OrderDataService($o);
            $service->create();
        }
        return TRUE;
    }

    /**
     * REST service. Returns json for Orders service.
     *
     * @param string $user
     * @param string $product
     * @return array
     */
    public function restGet($name = false, $product = false)
    {
        if ($name) {
            if (!is_string($name))
                return FALSE;
        }
        if ($product) {
            if (!is_string($product))
                return FALSE;
        }
        if (is_null($name))
            $name = FALSE;
        if (is_null($product))
            $product = FALSE;
        $service = new OrderDataService();
        $result = $service->restRead($name, $product);
        $arr = array();
        $i = 0;
        foreach ($result as $row) {
            $arr[$i++] = new Order($row["ID"], $row["USER"], $row["ORDER_DATE"],
                $row["PRODUCT_ID"], $row["PRODUCT"], $row["PRICE"],
                $row["CONTACT"], $row["QUANTITY"]);
        }
        return $arr;
    }

}