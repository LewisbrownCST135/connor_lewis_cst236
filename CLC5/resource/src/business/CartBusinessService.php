<?php
/*
Project: CST-236 CLC4 5.0
File: CartBusinessService.phpssService.php
Authors: Connor Low
Date: 10/27/17
Synopsis: Used to manage business rules in Cart
*/

class CartBusinessService
{
    private $user;

    /**
     * CartBusinessService constructor.
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Accesses CART table in DB and returns an array of products
     * @return array|bool
     */
    public function getProducts()
    {
        // check that the user isn't null
        if (is_null($this->user))
            return FALSE;

        // create Cart DS
        $service = new CartDataService($this->user);
        $cResult = $service->read();
        $productArray = array();

        // create $cart object from $service
        if ($cResult->num_rows > 0) {
            $i = 0;
            foreach ($cResult as $pRow) {

                // get the product from each row in the result and add to the array
                $product = new Product($pRow["ID"], $pRow["NAME"], $pRow["PRICE"], $pRow["DESCRIPTION"], $pRow["IMAGE"]);
                $productArray[$i++] = $product;
            }
        } else
            return FALSE;

        // result
        return $productArray;
    }

    /**
     * @param Product $product
     * @return bool|mysqli_result
     */
    public function addProduct(Product $product)
    {
        if ($this->validProduct($product)) {
            $service = new CartDataService($this->user);
            return $service->create($product);
        }
        return FALSE;
    }

    /**
     * @param Product $product
     * @return bool
     */
    private function validProduct(Product $product)
    {
        return $product instanceof Product && $product != null && $product->getName() != null && $product->getPrice() != null &&
            $product->getDescription() != null && $product->getImage() != null;
    }

    /**
     * @param Product $product
     * @return bool|mysqli_result
     */
    public function removeProduct(Product $product)
    {
        if ($product != null && $product->getId() > 0) {
            // TODO remove a product from the user cart
            $service = new CartDataService($this->user);
            return $service->delete($product, TRUE);
        }
        return FALSE;
    }

    /**
     * @return bool|mysqli_result
     */
    public function emptyCart()
    {
        // TODO remove all products from the user cart
        $service = new CartDataService($this->user);
        return $service->delete();
    }

}