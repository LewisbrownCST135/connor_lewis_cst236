<?php
/*
Project: CLC4 6.1
File: ProductBusinessService.php 2.0
Authors: Connor Low
Date: 12/8/17
Synopsis: Product Business Rules
*/

class ProductBusinessService
{
    private $product;
    private $service;

    /**
     * ProductBusinessService constructor.
     * @param $product
     */
    public function __construct($product = null)
    {
        $this->product = $product;
        if ($product != NULL)
            $this->service = new ProductDataService($product);
        else
            $this->service = new ProductDataService(new Product(-1));
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return bool|mysqli_result
     */
    public function newProduct()
    {
        if (is_null($this->product))
            return false;
        return $this->service->create();
    }

    /**
     * @param $cvcList
     * @return int
     */
    public function bulkAddProduct($cvcList)
    {
        // TODO add products to DB
        $compiler = new BulkCompile($cvcList);
        $compiler->toArray();
        $errors = $compiler->add();
        $array = $compiler->getProductArray();
        if ($array != NULL) {
            foreach ($array as $product) {
                $service = new ProductDataService($product);
                $service->create();
            }
        }
        return $errors;
    }

    /**
     * @return bool|mysqli_result
     */
    public function deleteProduct()
    {
        if (is_null($this->product))
            return false;
        return $this->service->delete();
    }

    /**
     * @return bool|mysqli_result
     */
    public function selectProduct()
    {
        if (is_null($this->product))
            return false;
        return $this->service->read();
    }

    /**
     * @return bool|mysqli_result
     */
    public function searchForProduct()
    {
        if (is_null($this->product))
            return false;
        return $this->service->read(TRUE);
    }

    /**
     * @return bool|mysqli_result
     */
    public function updateProduct()
    {
        if (is_null($this->product))
            return false;
        return $this->service->update();
    }

    /**
     * @param bool $name
     * @param bool $priceMin
     * @param bool $priceMax
     * @return array|bool
     */
    public function restGet($name = false, $priceMin = false, $priceMax = false)
    {
        if ($name) {
            if (!is_string($name))
                return FALSE;
        }
        if ($priceMin) {
            if (!is_string($priceMin))
                return FALSE;
        }
        if ($priceMax) {
            if (!is_string($priceMax))
                return FALSE;
        }
        if (is_null($name))
            $name = FALSE;
        if (is_null($priceMin))
            $priceMin = FALSE;
        if (is_null($priceMax))
            $priceMax = FALSE;
        $result = $this->service->restRead($name, $priceMin, $priceMax);
        $arr = array();
        $i = 0;
        foreach ($result as $row) {
            $arr[$i++] = new Product($row["ID"], $row["NAME"], $row["PRICE"], $row["DESCRIPTION"], $row["IMAGE"]);
        }
        return $arr;
    }

//    public function orderProduct($number, $date, $holder, $cv)
//    {
//            return strlen($this->number) != 16 ||
//                // trim($this->date) == "" ||
//                strlen($this->cv) != 3 ||
//                trim($this->holder) == "";
//    }


}
