<?php
/*
Project: CST-236 CLC4 3.0
File: secure page 2.0
Authors: Connor Low
Date: 10.5.17
Synopsis: checks to see if a user is logged in
*/
if (is_null($user)) {
    session_destroy();
    include "masterHandler.php";
}
?>
