<?php
/*
Project: CST-236 CLC4 5.0
File: header.php 4.0
Authors: Connor Low
Date: 10/22/17
Synopsis: starts session, references autoloader, creates user and cart
*/

$ini = parse_ini_file("../resource/config.ini", TRUE);
include_once $ini["Path"]["autoloader"];
session_start();

// create user object
$user = $_SESSION["user"];
if (isset($_SESSION["cart"]))
    $cart = $_SESSION["cart"];
