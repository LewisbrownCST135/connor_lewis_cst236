<?php
/*
Project: CST-236 CLC4 4.2
File: Cartold data service 1.0
Authors: Connor Low
Date: 10/22/17
Synopsis: CRUD for cart
*/

class CartDataService
{
    private $user;
    private $ini;

    /**
     * CartDSold constructor.
     * @param $product
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->ini = parse_ini_file("../resource/db.ini", TRUE);
    }

    /**
     * @return bool|mysqli_result
     * Use to css to cart
     */
    public function create(Product $product)
    {
        // query: get cart insert statement from ini file.
        $query = $this->ini['Insert']['cart'] . "('" .
            $product->getId() . "', '" .
            $this->user->getId() . "');";
        // execute query
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

    /**
     * @return bool|mysqli_result
     * Use to reveal cart contents
     */
    public function read()
    {
        // query: get cart select statement from file.
        // Inner join and select products related to USER.ID
        $query = $this->ini['Select']['cart'] . $this->user->getId() . ";";
        // execute query
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();

        return $result;
    }

    /*
     * @return bool|mysqli_result
     */
    public function delete(Product $product=null, $limit = FALSE)
    {
        // query: get cart select statement from file.
        // Inner join and select products related to USER.ID
        // setting $limit will only delete the first record. Use when a cart has more than one of the same product.
        if (is_null($product))
            $query = $this->ini['Delete']['cart-a'] . $this->user->getId() . ";";
        else {
            $query = $limit ?
                $this->ini['Delete']['cart-a'] . $this->user->getId() .
                $this->ini['Delete']['cart-b'] . $product->getId() . " limit 1;" :
                $this->ini['Delete']['cart-a'] . $this->user->getId() .
                $this->ini['Delete']['cart-b'] . $product->getId() . ";";
        }
        // execute query
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }


}