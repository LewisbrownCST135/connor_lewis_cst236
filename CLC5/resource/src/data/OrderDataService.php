<?php
/**
 * Created by PhpStorm.
 * User: speed
 * Date: 11/5/2017
 * Time: 4:24 PM
 */

class OrderDataService
{
    private $order;
    private $ini;

    /**
     * OrderDataService constructor.
     * @param $order
     */
    public function __construct(Order $order = null)
    {
        $this->order = $order;
        $this->ini = parse_ini_file("../resource/db.ini", TRUE);

    }

    /**
     * @return bool|mysqli_result
     */
    public function create()
    {
        $query = $this->ini['Insert']['order'] . "('" .
            $this->order->getUser() . "', '" .
            $this->order->getPId() . "', '" .
            $this->order->getProduct() . "', '" .
            $this->order->getPrice() . "', '" .
            $this->order->getContact() . "', '" .
            $this->order->getQuantity() . "', NOW());";
        // execute query
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

    /**
     * $order must be set.
     * @return bool|mysqli_result
     */
    public function read()
    {
        $query = $this->ini['Select']['order'] .
            "USER like '%" . $this->order->getUser() . "%' and " .
            "PRODUCT like '%" . $this->order->getProduct() . "%';";

        // execute query
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

    public function getCount($distinct)
    {
        $query = $distinct ? $this->ini['Other']['count-d'] : $this->ini['Other']['count'];

        // execute query
        $connection = new Connection();
        $result = $connection->connect()->query($query)->fetch_assoc();
        $connection->disconnect();
        return $result["COUNT"];
    }

    /**
     * @param bool $quantity
     * @return bool|mysqli_result
     */
    public function readProductReport($page, $quantity = false)
    {
        $query = $quantity ? $this->ini['Other']['productCount'] . $this->order->getPId() :
            $this->ini['Other']['productReport'] . $this->order->getPId() . " limit $page, 10";

        // execute query
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

    /**
     * @return bool|mysqli_result
     * @param $page
     */
    public function readPrimaryReport()
    {
        $query = $this->ini['Other']['primaryReport'];
        // execute query
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

    public function getQuantity()
    {
        $query = $this->ini['Select']['order'] .
            "USER like '%" . $this->order->getUser() . "%' and " .
            "PRODUCT like '%" . $this->order->getProduct() . "%';";

        // execute query
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return array|bool
     */
    public function getIni()
    {
        return $this->ini;
    }

    /**
     * @param array|bool $ini
     */
    public function setIni($ini)
    {
        $this->ini = $ini;
    }

    public function restRead($name, $product)
    {
        $query = "select * from ORDERS";
        $condition = " where ";
        $proto = 0;
        if ($name) {
            $condition .= "USER like '%$name%' ";
            $proto += 2;
        }
        if ($product) {
            if ($proto != 0)
                $condition .= " and ";
            $condition .= "PRODUCT like '%$product%'";
            $proto += 3;
        }
        $query .= ($proto > 0) ? $condition . ";" : ";";
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }
}