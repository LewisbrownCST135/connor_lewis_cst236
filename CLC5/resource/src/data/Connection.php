<?php
/*
Project: CST-236 CLC4 3.01
File: Connection.php 3.0
Authors: Connor Low
Date: 10/4/17
Synopsis: Used to enter credentials to be checked against the DB
 */

class Connection
{
    private $server;
    private $username;
    private $password;
    private $database;
    private $connection;
    private $ini;

    /**
     * Connection constructor.
     * uses ini file to initialize database credentials
     */
    public function __construct()
    {
        $this->ini = parse_ini_file("../resource/db.ini", TRUE);
        $this->server = $this->ini['DB']['server'];
        $this->username = $this->ini['DB']['username'];
        $this->password = $this->ini['DB']['password'];
        $this->database = $this->ini['DB']['database'];
        $this->connection = new mysqli();
    }

    /**
     * @return mysqli
     * returns a connection to the database
     */
    public function connect()
    {
        $this->connection = new mysqli($this->server, $this->username, $this->password, $this->database);
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        // Check
        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        }
        return $this->connection;
    }

    /**
     * closes the connection
     */
    public function disconnect()
    {
        $this->connection->close();
    }

}