<?php

class ProductDataService
{
    private $product;
    private $ini;

    /**
     * Product_DS constructor.
     * @param $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
        // get database statement configuration file: config.ini
        $this->ini = parse_ini_file("../resource/db.ini", TRUE);
    }

    /**
     * @return bool|mysqli_result
     */
    public function create()
    {
        // query: get product insert statement from ini file.
        $query = $this->ini['Insert']['product'] . "('" .
            $this->product->getName() . "', '" .
            $this->product->getDescription() . "', '" .
            $this->product->getPrice() . "', '" .
            $this->product->getImage() . "');";
        // execute query
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

    /**
     * @param bool $likeName
     * @return bool|mysqli_result
     */
    public function read($likeName = FALSE)
    {
        // the where clause is determined. A user may search by name (likeName == TRUE)
        // and an admin may retrieve products by ID (likeName == FALSE)
        $where = $likeName ? " NAME like '%" . $this->product->getName() . "%'" :
            " ID = '" . $this->product->getId() . "'";
        // query: get product select statement from ini file and append $where on the end.
        $query = $this->ini['Select']['product'] . $where . ";";
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

    /**
     * @return bool|mysqli_result
     */
    public function update()
    {
        // use update product statement from ini.
        // Updates a product of a specific ID.
        $query = $this->ini['Update']['product'] .
            "NAME = '" . $this->product->getName() . "', " .
            "IMAGE = '" . $this->product->getImage() . "', " .
            "DESCRIPTION = '" . $this->product->getDescription() . "', " .
            "PRICE = '" . $this->product->getPrice() . "' " .
            "where ID = '" . $this->product->getId() . "';";
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

    /**
     * @return bool|mysqli_result
     */
    public function delete()
    {
        // use delete product statement from ini.
        // Deletes a product of a specific ID.
        $query = $this->ini['Delete']['product']
            . "ID = '" . $this->product->getId() . "';";
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

    public function restRead($name = false, $priceMin = false, $priceMax = false)
    {
        $query = "select * from PRODUCT";
        $condition = " where ";
        $proto = 0;
        if ($name) {
            $condition .= "NAME like '%$name%' ";
            $proto += 2;
        }
        if ($priceMin) {
            if ($proto != 0)
                $condition .= " and ";
            $condition .= "PRICE > '$priceMin' ";
            $proto += 3;
        }
        if ($priceMax) {
            if ($proto != 0)
                $condition .= " and ";
            $condition .= "PRICE < '$priceMax'";
            $proto += 5;
        }
        $query .= ($proto > 0) ? $condition . ";" : ";";
        $connection = new Connection();
        $result = $connection->connect()->query($query);
        $connection->disconnect();
        return $result;
    }

}