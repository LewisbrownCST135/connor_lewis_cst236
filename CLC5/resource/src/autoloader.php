<?php
/*
Project: CST-236 CLC4 4.01
File: autoloader.php 2.0
Authors: Connor Low
Date: 10/23/17
Synopsis: Class loader
Resources: https://www.sitepoint.com/autoloading-and-the-psr-0-standard/
 */

function autoloadDS($class)
{
    $filename = __DIR__ . "/data/" . $class . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}

function autoloadBR($class)
{
    $filename = __DIR__ . "/business/" . $class . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}

function autoloadModel($class)
{
    $filename = __DIR__ . "/model/" . $class . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}

spl_autoload_register('autoloadDS');
spl_autoload_register('autoloadBR');
spl_autoload_register('autoloadModel');
