<?php

/*
Project: CST-236 CLC4 5.0
File: Payment Service 1.0
Authors: Connor Low
Date: 10/26/17
Synopsis: Hold Payment information and handles it
 */

/**
 * Class PaymentService
 * Service is responsible for
 */
class PaymentService
{
    // User object holds all payment information: Card Number, CVC, Expiration, Card Holder, Billing Address
    private $user;

    // Double or float containing total cost of transaction
    private $orderTotal;

    // Used to authorize payment. Set via the authorize function.
    private $authorizationToken;

    /**
     * PaymentService constructor.
     * @param User $user
     * @param $orderTotal
     */
    public function __construct(User $user, $orderTotal)
    {
        $this->user = $user;
        $this->orderTotal = $orderTotal;
    }


    /**
     * Validates card and authorizes transaction
     * return authorizationToken|bool
     */
    public function authorize()
    {
        // send User payment information (Card) from payment form to Payment REST API
        // if information was invalid, return false

        // get a secure representation token from the REST API. store in $authorizationToken field

        return TRUE; // placeholder
    }

    /**
     * Used to charge a card and capture a payment
     * @return bool
     */
    public function order()
    {
        // get $authorizationToken

        // create charge array containing the payment amount, currency, description, and $authorizationToken

        // capture payment (finalizes the transaction)

        return TRUE; // placeholder
    }

    /**
     * @return bool
     */
    public function receipt()
    {
        // update RECORD table in database

        // send an email to user

        return TRUE; // placeholder
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param mixed $orderTotal
     */
    public function setOrderTotal($orderTotal)
    {
        $this->orderTotal = $orderTotal;
    }
}