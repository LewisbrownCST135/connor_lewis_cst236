<?php

class BulkCompile
{
    private $input;
    private $output;
    private $col1;
    private $col2;
    private $col3;
    private $col4;
    private $productArray = array();

    /**
     * Bulk_Compile constructor.
     * @param $input
     * @param $col1
     * @param $col2
     * @param $col3
     * @param $col4
     */
    public function __construct($input)
    {
        $this->input = $input;
    }


    public function toArray($columnDiv = ",", $rowDiv = ";")
    {
        // store column names in variables
        $columns = strstr($this->input, ";", true);
        $this->col1 = strstr($columns, ",", true);
        $columns = strstr(substr($columns, 1), ",");
        $this->col2 = strstr(substr($columns, 1), ",", true);
        $columns = strstr(substr($columns, 1), ",");
        $this->col3 = strstr(substr($columns, 1), ",", true);
        $this->col4 = substr(strstr(substr($columns, 1), ","), 1);

        // remove column names from query 
        $this->input = strstr($this->input, ";");
        $array = explode($rowDiv, trim($this->input, ";"));

        foreach ($array as $i => $row) {
            $array[$i] = explode($columnDiv, $row);
        }
        $this->output = $array;
    }

    /**
     * @return int (number of errors)
     */
    public function add()
    {
        $lines = 0;
        $success = 0;
        foreach ($this->output as $row) {
            $product = new Product(null);
            // set attributes
            if ($this->setAttribute($product, trim($this->col1), $row[0]))
                if ($this->setAttribute($product, $this->col2, $row[1]))
                    if ($this->setAttribute($product, $this->col3, $row[2]))
                        if ($this->setAttribute($product, $this->col4, $row[3]))
                            $this->productArray[$success++] = $product;
            $lines++;
        }
        return $lines - $success;
    }

    /**
     * @param Product $product
     * @param $property
     * @param $value
     * @return bool
     */
    private function setAttribute(Product $product, $property, $value)
    {
        $value = str_replace("\r\n", "", $value);
        // determine which property to set in product
        if ($value != "")
            switch (strtoupper($property)) {
                case "NAME":
                    $product->setName($value);
                    break;
                case "PRICE":
                    $product->setPrice($value);
                    break;
                case "DESCRIPTION":
                    if ($pos = strstr($this->input, "[")) {
                        $product->setDescription(strstr($pos, "]"));
                    } else
                        $product->setDescription($value);
                    break;
                case "IMAGE":
                    $product->setImage($value);
                    break;
                default:
                    // catch errors (incorrect property defined);
                    return false;
            }
        else
            return false;
        return true;
    }

    /**
     * @return array
     */
    public function getProductArray()
    {
        return $this->productArray;
    }


}