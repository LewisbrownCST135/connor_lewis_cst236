<?php
/*
Project: CST-236 CLC4 4.2
File: Cart 1.1
Authors: Connor Low
Date: 10/22/17
Synopsis: holds an array of products
*/

class Cart
{
    private $productArray = array();
    private $itemCount = 0;

    /**
     * Cart constructor.
     * @param array $productArray
     */
    public function __construct(array $productArray)
    {
        $this->productArray = $productArray;
        $this->itemCount = count($productArray);
    }

    /**
     * @return array
     */
    public function getProductArray()
    {
        return $this->productArray;
    }

    /**
     * @param array $productArray
     */
    public function setProductArray($productArray)
    {
        $this->productArray = $productArray;
        $this->itemCount = count($productArray);
    }

    /**
     * @return int
     */
    public function getItemCount()
    {
        return $this->itemCount;
    }

}