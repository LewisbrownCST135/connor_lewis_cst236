<?php

/*
Project: CST-236 CLC4 5.0
File: User.php 2.0
Authors: Connor Low
Date: 10/26/17
Synopsis: Holds basic user membership information at Login
 */

class User
{
    private $id;
    private $username;
    private $password;
    private $firstName;
    private $lastName;
    private $email;
    private $admin;
    private $cardN;
    private $cardCVC;
    private $cardED;
    private $cardH;
    private $billingAddress;
    private $address;

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * User2 constructor.
     * @param $id
     * @param $username
     * @param $password
     * @param $firstName
     * @param $lastName
     * @param $email
     * @param $admin
     * @param $cardN
     * @param $cardCVC
     * @param $cardED
     * @param $cardH
     * @param $billingAddress
     */
    public function __construct($id, $username, $password, $firstName = "", $lastName = "",
                                $email = "", $admin = FALSE, $cardN = "", $cardCVC = "",
                                $cardED = null, $cardH = "", $billingAddress = "")
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->admin = $admin;
        $this->cardN = $cardN;
        $this->cardCVC = $cardCVC;
        $this->cardED = $cardED;
        $this->cardH = $cardH;
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return mixed
     */
    public function getCardN()
    {
        return $this->cardN;
    }

    /**
     * @param mixed $cardN
     */
    public function setCardN($cardN)
    {
        $this->cardN = $cardN;
    }

    /**
     * @return mixed
     */
    public function getCardCVC()
    {
        return $this->cardCVC;
    }

    /**
     * @param mixed $cardCVC
     */
    public function setCardCVC($cardCVC)
    {
        $this->cardCVC = $cardCVC;
    }

    /**
     * @return mixed
     */
    public function getCardED()
    {
        return $this->cardED;
    }

    /**
     * @param mixed $cardED
     */
    public function setCardED($cardED)
    {
        $this->cardED = $cardED;
    }

    /**
     * @return mixed
     */
    public function getCardH()
    {
        return $this->cardH;
    }

    /**
     * @param mixed $cardH
     */
    public function setCardH($cardH)
    {
        $this->cardH = $cardH;
    }

    /**
     * @return mixed
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param mixed $billingAddress
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }


}