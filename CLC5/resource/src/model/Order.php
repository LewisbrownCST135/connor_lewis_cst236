<?php

class Order implements JsonSerializable
{
    private $id, $user, $orderDate, $pId, $product, $price, $contact, $quantity;

    /**
     * Order constructor.
     * @param $id
     * @param $user
     * @param $orderDate
     * @param $pId
     * @param $product
     * @param $price
     * @param $contact
     * @param $quantity
     */
    public function __construct($id = -1, $user = "", $orderDate = -1, $pId = -1, $product = "", $price = -1, $contact = "", $quantity = 1)
    {
        $this->id = $id;
        $this->user = $user;
        $this->orderDate = $orderDate;
        $this->pId = $pId;
        $this->product = $product;
        $this->price = $price;
        $this->contact = $contact;
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param mixed $orderDate
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;
    }

    /**
     * @return mixed
     */
    public function getPId()
    {
        return $this->pId;
    }

    /**
     * @param mixed $pId
     */
    public function setPId($pId)
    {
        $this->pId = $pId;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }


    function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
