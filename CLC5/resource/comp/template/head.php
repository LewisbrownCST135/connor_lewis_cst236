<?php
/*
Project: CST-236 CLC4 5.0
File: head.php 2.0
Authors: Connor Low
Date: 10/26/17
Synopsis: Sets title and loads styles and js
*/

?>
<head>
    <title><?php echo $title; ?></title>
    <link href="css/styles.css" rel="stylesheet">
    <!--Scripts go here-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/script.js"></script>
</head>