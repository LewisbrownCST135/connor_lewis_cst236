<?php
/*
Project: CST-236 CLC4 3.2
File: <header> comp 3.0
Authors: Connor Low
Date: 10/8/17
Synopsis: comp to load for page header
*/
?>
<header>
    <div id="nav_container">
        <nav id="main_nav">
            <div id="main_links">
                <div class="LeftNav">
                    <a href="Home.php">Home</a>
                    <a href="masterHandler.php">Logout</a>
                    <?php
                    if ($user->getAdmin() == 1) {
                        echo "<a href=\"AdminTools.php\">Administration</a>";
                    }
                    ?>
                </div>
                <div class="RightNav">
                    <div class="Container">
                        <div class="LeftNav">

                            <form id="search" method="post" action="masterHandler.php?user_search">
                                Search: <input name="NAME" type="text"/>
                                <input name="Search" value="Search" type="submit"/>
                            </form>
                        </div>
                        <div class="RightNav">
                            <a href="CartView.php">Cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
