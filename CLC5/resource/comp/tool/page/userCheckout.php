<?php
/**
 * @var $user User
 * @var $product Product
 */
$cartService = new CartBusinessService($user);
if ($cartService->getProducts()) {
    echo "<ul class=\"CartContent\">";
    foreach ($cartService->getProducts() as $product) {
        ?>
        <li class="CartItem">
            <div class="Container">
                <div class="CartName">
                    <h3><?php echo $product->getName() ?></h3>
                </div>
            </div>
            <div class="Container">
                <div class="CartPrice">
                    <p>$<?php echo $product->getPrice() ?></p>
                </div>
            </div>
        </li>
        <?php
        $totalCost += (double)$product->getPrice();
    }
    echo "</ul>";
    $_SESSION["TC"] = $totalCost;
    ?>
    <div class="TotalCost">
        <p>Total Cost: $<?php echo $totalCost; ?></p>
    </div>
    <?php
} else {
    ?>
    <p>Cart is empty :(</p>
    <?php
    $empty = true;
}