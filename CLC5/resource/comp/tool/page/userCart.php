<?php
/*
Project: CST-236 CLC5 5.1
File: loginHandler.php 2.0
Authors: Connor Low, Lewis Brown
Date: 11/2/17
Synopsis: Loads cart contents to CartView.php
*/
/**
 * @var $user User
 */
$cartService = new CartBusinessService($user);
if ($cartService->getProducts()) {
    /**
     * @var $product Product
     */
    echo "<ul class=\"CartContent\">";
    foreach ($cartService->getProducts() as $product) {
        ?>
        <li class="CartItem">
            <div class="CartImage">
                <img width="90%" <?php echo "alt='img/" . $product->getImage() .
                    "' src ='img/" . $product->getImage() . "'" ?>>
            </div>
            <div class="Container">
                <div class="CartName">
                    <h3><?php echo $product->getName() ?></h3>
                </div>
                <div class="CartPrice">
                    <p>$<?php echo $product->getPrice() ?></p>
                </div>
                <a <?php echo "href='CartView.php?action=delete&productId=" .
                    $product->getId() . "' "; ?>>
                    <div class="RemoveProduct">
                        <p>Remove</p>
                    </div>
                </a>
            </div>
        </li>
        <?php
    }
    echo "</ul>";
} else
    echo "<br/> Empty :(";