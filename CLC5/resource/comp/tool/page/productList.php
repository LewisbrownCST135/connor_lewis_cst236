<?php
$id = 0;
$name = isset($_POST["NAME"]) ? $_POST["NAME"] : "";
$product = new Product($id, $name, NULL, NULL, NULL);
$service = new ProductBusinessService($product);
$result = $service->searchForProduct();
echo "<ul>";
foreach ($result as $row) {
    ?>

    <li class="CatalogResult">
        <div class="ProductImage">
            <a <?php echo "href='ProductView.php?" . $row['ID'] . "' "; ?>>
                <img height="150px" <?php echo "alt='img/" . $row["IMAGE"] . "' src ='img/" . $row["IMAGE"] . "'" ?>>
            </a>
        </div>
        <div class="ProductName">
            <a <?php echo "href='ProductView.php?" . $row['ID'] . "' "; ?>>
                <h3><?php echo $row["NAME"] ?></h3>
            </a>
        </div>
        <div class="ProductDescription" >
            <p>
                <?php
                $visibleText = substr($row["DESCRIPTION"], 0, 100);
                if (strlen($row["DESCRIPTION"]) > 100) {
                    $visibleText = substr($visibleText, 0, 97);
                    $visibleText .= " ... ";
                }
                echo $visibleText;
                ?>
            </p>
        </div>
        <div class="ProductPrice">
            <div class="InactiveBtn">
                <?php echo "<p>$" . $row["PRICE"] . "</p>" ?>
            </div>
            <div class="ActiveBtn">
                <?php echo "<a href='CartView.php?action=add&productId=" .
                    $row["ID"] . "' title='Add to Cart'>Add to Cart</a>"; ?>
            </div>
        </div>
    </li>

    <?php
}
echo "</ul>";
?>

