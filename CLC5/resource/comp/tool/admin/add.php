<?php
/*
Project: CST-236 CLC4 4.1
File: add 1.0
Authors: Connor Low
Date: 10/21/17
Synopsis: Loads the admin css tool
*/

?>

<div class="Title">
    <h3>ADD Product: <?php echo $row["NAME"] ?></h3>
</div>
<div class="Instruction">
</div>
<div class="ProductForm">

    <form method="post" action="masterHandler.php?admin_addProduct">
        <p>Name:</p>
        <input title="name" class="edit" name="name" maxlength="100"/><br/>
        <p>Image name:</p>
        <input title="image" class="edit" name="image" maxlength="255"/><br/>
        <p>Description:</p>
        <input title="description" class="edit" name="description" maxlength="2000" /><br/>
        <p>Price:</p>
        <input title="price" class="edit" name="price"  /><br/><br/>
        <input type="submit" value="Update">
    </form>
</div>
