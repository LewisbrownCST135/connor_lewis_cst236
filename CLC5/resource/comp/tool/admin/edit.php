<?php
/*
Project: CST-236 CLC4 4.1
File: edit 1.0
Authors: Connor Low
Date: 10/21/17
Synopsis: Used update a selected product
*/
$id = $_GET["id"];
$productDS = new ProductDataService(new Product($id));
$row = $productDS->read()->fetch_assoc();


if ($row != NULL) { ?>

<div class="Title">
    <h3>Edit Product: <?php echo $row["NAME"] ?></h3>
</div>
<div class="Instruction">
</div>
<div class="ProductForm">
    <h3>Image:</h3>
    <img width="100px" src= <?php echo "\"img/" . $row["IMAGE"] . "\"" ?>>
    <h3>Properties:</h3>
    <form method="post" action="masterHandler.php?admin_updateProduct">
        <p>ID:</p>
        <input class="Readonly" title="id" name="id" readonly value= <?php
        echo "\"" . $row["ID"] . "\"";
        ?>><br/>
        <p>Name:</p>
        <input title="name" class="edit" name="name" maxlength="100" value= <?php
        echo "\"" . $row["NAME"] . "\"";
        ?> /><br/>
        <p>Image name:</p>
        <input title="image" class="edit" name="image" maxlength="255" value= <?php
        echo "\"" . $row["IMAGE"] . "\"";
        ?>/><br/>
        <p>Description:</p>
        <input title="description" class="edit" name="description" maxlength="2000" value= <?php
        echo "\"" . $row["DESCRIPTION"] . "\"";
        ?>/><br/>
        <p>Price:</p>
        <input title="price" class="edit" name="price" value= <?php
        echo "\"" . $row["PRICE"] . "\"";
        ?>/><br/><br/>
        <input type="submit" value="Update">
    </form>
    <h3 id="delete">Delete product</h3>
    <form method="post" action="masterHandler.php?admin_deleteProduct">
        <div class="hidden" style="visibility: collapse">
            <input readonly title="" name="id" value=
                <?php echo '"' . $row["ID"] . '"'; ?>>
        </div>
        <input type='submit' value="Delete">
    </form>

    <?php
    } else {
        ?>

        <h3>Error: no product selected.</h3>

    <?php } ?>
</div>