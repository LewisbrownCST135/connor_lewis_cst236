<?php
/*
Project: CST-236 CLC4 4.1
File: find 1.0
Authors: Connor Low
Date: 10/21/17
Synopsis: loads the admin find tool.
*/
?>
<div class="Title">
    <h3>Product Search Tool</h3>
</div>
<div class="Instruction">
</div>
<div class="SearchForm">
    <h3>Criteria:</h3>
    <form method="post" action="masterHandler.php?admin_findProduct">
        <div class="SubForm">
            <p>Name:</p>
            <input title="name" class="edit" name="name" maxlength="100"/><br/>
        </div>
        <div class="SubForm">
            <input type="submit" value="Search">
        </div>
    </form>
</div>
