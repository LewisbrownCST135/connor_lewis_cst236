<?php
/*
Project: CST-236 CLC 6.2
File: infor 2.0
Authors: Connor Low
Date: 12/10/17
Synopsis: Loads the business report for a specific product
*/

$page = strlen(trim($_GET["page"])) > 0 ? $_GET["page"] : 0;
$productService = new ProductBusinessService(new Product($_GET["id"]));
$result = $productService->selectProduct()->fetch_assoc();
$service = new OrderBusinessService(new Order($_GET["id"]));
$productReport = $service->getProductReport($_GET["id"], $page * 10);
$total = $service->getProductReport($_GET["id"], $page * 10, true)->fetch_assoc();

?>

<div class="Title">
    <h3>Sales report for: [<?php echo $result["ID"] . "] " . $result["NAME"]; ?></h3>
    <h3><?php echo "$" . $result["PRICE"]; ?></h3>
    <p><?php echo $result["DESCRIPTION"]; ?></p>
    <a <?php echo "href=\"AdminTools.php?action=edit&id=" . $result["ID"] . "\""; ?>>edit</a>
</div>
<div class="Instruction">
</div>
<div class="BusinessReport">
    <table class="ReportTable">
        <tr class="ReportEven">
            <th class="ReportHeader"> Order No.</th>
            <th class="ReportHeader"> User</th>
            <th class="ReportHeader"> User Info</th>
            <th class="ReportHeader"> Date</th>
            <th class="ReportHeader"> Quantity (total: <?php echo $total["COUNT"]; ?>)</th>
        </tr>
        <?php
        /**
         * @var $order Order
         */
        while ($order = $productReport->fetch_assoc()) {
            ?>
            <tr class="ReportEven">
                <td class="ReportCell" style="width: 50px">
                    <?php echo $order["ID"]; ?>
                </td>
                <td class="ReportCell">
                    <?php echo $order["USER"] ?>
                </td>
                <td class="ReportCell">
                    <?php echo $order["CONTACT"] ?>
                </td>
                <td class="ReportCell">
                    <?php echo $order["ORDER_DATE"] ?>
                </td>
                <td class="ReportCell" style="width: 150px">
                    <?php echo $order["QUANTITY"] ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php if ($page > 0)
        echo "<a style='float:left' href='AdminTools.php?action=info&page=" . ($page - 1) .
            "&id=" . $_GET["id"] . "'>Back</a>  "; ?>
    <?php if ($service->getProductReport($_GET["id"], 10 * ($page + 1)))
        echo "<a style='float:right' href='AdminTools.php?action=info&page=" . ($page + 1) .
            "&id=" . $_GET["id"] . "'>Next</a>  "; ?>
    <br/ >
</div>
