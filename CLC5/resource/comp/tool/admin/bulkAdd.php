<?php
/*
Project: CST-236 CLC4 4.1
File: bulkAdd 1.0
Authors: Connor Low
Date: 10/21/17
Synopsis: Loads the bulk-add tool.
*/
?>
<div class="Title">
    <h3>Bulk Add Products</h3>
</div>
<div class="Instruction">
    <p>Must use these 4 columns: NAME, PRICE, IMAGE, DESCRIPTION</p>
    <p>Must end row in ";"</p>
    <p>Separate columns with a ","</p>
</div>
<div class="BulkForm">
    <form action="masterHandler.php?bulkAddProduct" method="post" id="bulk">
        <div class="SubForm">
            <textarea title="Entry" name="list" form="bulk"></textarea>
        </div>
        <div class="SubForm">
            <input type="submit" value="Import"/>
        </div>
    </form>
</div>
