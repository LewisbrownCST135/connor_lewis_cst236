<?php
/*
Project: CST-236 CLC4 6
File: report 2.0
Authors: Connor Low
Date: 12/10/17
Synopsis: Loads the business report
*/
$page = strlen(trim($_GET["page"])) > 0 ? $_GET["page"] : 0;
?>

<div class="Title">
    <h3>Business Report: <?php echo $row["NAME"] ?></h3>
</div>
<div class="BusinessReport">
    <table class="ReportTable">
        <tr class="ReportEven">
            <th class="ReportHeader">ID</th>
            <th class="ReportHeader">Name</th>
            <th class="ReportHeader">Sales</th>
        </tr>
        <?php
        /**
         * @var $order Order
         */
        $service = new OrderBusinessService();
        $report = $service->getPrimaryReport($page);
        $rowN = 1;
        if ($report == -1)
            $report = $service->getPrimaryReport(0);
        foreach ($report as $order)
            if (!is_null($order)) {
                $rowType = $rowN++ % 2 == 0 ? "ReportEven" : "ReportOdd";
                ?>
                <tr <?php echo "class='$rowType'"; ?>>
                    <td class="ReportCell">
                        <?php echo $order->getPId(); ?>
                    </td>
                    <td class="ReportCell">
                        <?php
                        echo("
                        <a href = 'AdminTools.php?action=info&page=0&id=" . $order->getPId() . "' >" .
                            "<div class='Container' >" . $order->getProduct() . "</div >
                        </a >
                        ");

                        ?>
                    </td>
                    <td class="ReportCell">
                        <?php echo $order->getQuantity(); ?>
                    </td>
                </tr>
                <?php
            }
        ?>
    </table>
    <br>
    <?php if ($page > 0)
        echo "<a style='float:left' href='AdminTools.php?action=report&page=" . ($page - 1) . "'>Back</a>  "; ?>
    <?php if ($service->getCount(true) - (10 + $page * 10) > 0)
        echo "<a style='float:right' href='AdminTools.php?action=report&page=" . ($page + 1) . "'>Next</a>"; ?>
</div>
