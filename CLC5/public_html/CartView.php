<?php

/*
Project: CST-236 CLC4 4.3
File: CartView.php 2.1
Authors: Connor Low
Date: 10/22/17
Synopsis: View cart contents
*/

$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["header"];
require_once $ini["Path"]["secure"];
// determine if cart has been modified
if (isset ($_SERVER["QUERY_STRING"]) && trim($_SERVER["QUERY_STRING"]) != "") {
    // determine action from query string
    $action = $_GET["action"];

    // include appropriate handler
    if ($action == "add") {
        include $ini["Path"]["handler"] . "addToCart.php";
    } elseif ($action == "delete") {
        include $ini["Path"]["handler"] . "deleteFromCart.php";
    }
}
?>

<!DOCTYPE HTML>
<html lang="en">
<?php
$title = "Retro Gamer - Cart";
include $ini["Template"]["head"];
?>

<body>
<?php include $ini["Template"]["nav"]; ?>
<div id="main_content">
    <div class="CenterContent">
        <div class="Title">
            <h3>Cart for <?php echo $user->getFirstName() ?></h3>
        </div>
        <div class="CartContent">
            <!-- links to /resource/comp/tool/page/userCart.php -->
            <?php include $ini["Tool"]["cart"] ?>

        </div>
        <div class="CheckoutStep">
            <a href="Home.php">Continue Shopping</a></div> |
        <div class="CheckoutStep">
            <a href="Checkout.php">Checkout</a>
        </div>
    </div>
</div>
<?php
include $ini["Template"]["foot"];
?>
</body>
</html>