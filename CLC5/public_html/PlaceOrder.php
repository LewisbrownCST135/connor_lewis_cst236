<?php

/*
Project: CST-236 CLC4 4.3
File: PlaceOrder.php 1.0
Authors: Connor Low
Date: 10/22/17
Synopsis: Place Order of products
*/
$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["header"];
require_once $ini["Path"]["secure"];
$totalCost = $_SESSION["TC"];

// TODO create Card object to get card number etc

// TODO create a CardBusinessService

// TODO return card information (encapsulate a CardDataService in your BusinessService)

// TODO use information to create a Card

?>

<!DOCTYPE HTML>
<html lang="en">
<?php
$title = "Confirm - Retro Gamer";
include $ini["Template"]["head"];
?>

<body>
<?php include $ini["Template"]["nav"]; ?>
<div id="main_content">
    <div class="CenterContent">
        <div class="Title">
            <p>Checkout for <?php echo $user->getFirstName() ?>.</p>
        </div>
        <div class="Container">
            <div class="SubTotal">
                <h3>Confirm order for total: $<?php echo $_SESSION["TC"] ?></h3>
            </div>
            <div class='Errors'>
                <div class="Container">
                    <?php
                    //<!--isset checks is a variable is set and is not null.it also returns true or false-->
                    //$error is from loginFORM
                    if (isset($error)) {
                        echo "ERROR: invalid credentials. Make sure you entered in your card " .
                            "correctly (must be 16 characters long)";
                    }
                    ?>
                </div>
            </div>
            <div class="FormContainer">
                <form action="masterHandler.php?placeOrder" method="Post">
                    <div class="SubForm">
                        <h3>Card Information</h3>
                        <input title="Card" name="card" type="password"
                               placeholder="0000111122223333" maxlength="16"
                            <?php echo "value='" . $user->getCardN() . "'" ?>
                        >
                        <input title="Card CVC" name="cvc" placeholder="CVC" class="CardBit" maxlength="3"
                            <?php echo "value='" . $user->getCardCVC() . "'" ?>
                        ><br/>
                        <input title="Card Expiration" type="date" name="date" maxlength="10"
                               placeholder="10/11/12"
                            <?php echo "value='" . date($user->getCardED()) . "'" ?>
                        ><br/>
                        <input title="Card Holder" name="holder" maxlength="100"
                               placeholder="Name on Card"
                            <?php echo "value='" . $user->getCardH() . "'" ?>
                        ><br/>
                        <input title="Billing Address" name="billing" maxlength="100"
                               placeholder="Billing Address"
                            <?php echo "value='" . $user->getBillingAddress() . "'" ?>
                        ><br/>
                    </div>
                    <div class="SubForm">
                        <h3>Shipping Information</h3>
                        <input title="Street" name="shipping1" placeholder="Shipping address line 1" maxlength="50"><br>
                        <input title="City, State, Zip" name="shipping2" placeholder="Shipping address line 2" maxlength="50">
                    </div>
                    <input class="Button" type="submit" value="Confirm">
                </form>
            </div>
        </div>
        <div class="CheckoutStep">
            <a href="Checkout.php">Back to Checkout</a>
        </div>
    </div>
</div>
<?php
include $ini["Template"]["foot"];
?>
</body>
</html>