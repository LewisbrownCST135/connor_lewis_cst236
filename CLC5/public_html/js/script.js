$(document).ready(function () {
    var handler = function () {
        if (parseFloat($('#main_content').height()) < screen.height - 300) {
            $('footer').addClass('Small');
            $('body').css({
                "height" : "100%",
                "position" : "absolute"
            });
        }
        else {
            $('footer').removeClass('Small');
        }
    };

    handler();
    $(window).resize(handler);
//    $('div.debug').append("<p>html height: " + $hh);
});