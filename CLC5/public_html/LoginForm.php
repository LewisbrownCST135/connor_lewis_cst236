<?php
/*
Project: CST-236 CLC4 6.0
File: LoginForm.php 2.0
Authors: Connor Low, Lewis Brown
Date: 11/23/17
Synopsis: Used to enter credentials to be checked against the DB
*/
$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["autoloader"];

?>
<!DOCTYPE HTML>
<html lang="en">
<?php
$title = "Login to RetroGamer";
include $ini["Template"]["head"];
?>

<body>
<div id="main_content">
    <div id="main_nav">
        <a href="index.html">Retro Gamer</a>
    </div>
    <div class="CenterContent">
        <div class='Errors'>
            <div class="Container">
                <?php
                //<!--isset checks is a variable is set and is not null.it also returns true or false-->
                //$error is from loginFORM
                if (isset($error)) {
                    //checks if error is = to 1
                    if ($error == 1)
                        echo "<p>Please enter both Username and Password</p>";
                    //checks if error is = to 2
                    if ($error == 2)
                        echo "<p>Incorrect username or password</p>";
                }
                ?>
            </div>
        </div>
        <div class="FormContainer">
            <form action="masterHandler.php?login" method="Post">
                <div class="SubForm">
                    <input title="Username" name="username"
                           placeholder="Username" maxlength="30"><br/>
                </div>
                <div class="SubForm">

                    <input title="Password" type="password" name="password"
                           placeholder="Password" maxlength="50"><br/>
                    <input class="Button" type="submit" value="Log In">
                </div>
            </form>
        </div>
        <div class="PostForm">
            <div class="Container">
                <a href="RegisterForm.php">New User? Register here.</a>
            </div>
        </div>
    </div>
</div>
<?php
include $ini["Template"]["foot"];
?>
</body>
</html>