<?php

/*
Project: CST-236 CLC4 4.3
File: Checkout.php 1.0
Authors: Connor Low
Date: 10/22/17
Synopsis: Checkout products
*/
$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["header"];
require_once $ini["Path"]["secure"];
$totalCost = 0;
?>

<!DOCTYPE HTML>
<html>
<?php
$title = "Checkout - Retro Gamer";
include $ini["Template"]["head"];
?>

<body>
<?php include $ini["Template"]["nav"]; ?>
<div id="main_content">
    <div class="CenterContent">
        <div class="Title">
            <p>Order successful!</p>
        </div>
        <div class="CheckoutStep">
            <?php echo "<ul class=\"CartContent\">";
            /**
             * @var $product Product
             */
            foreach ($_SESSION["lastOrder"] as $product) {
                ?>
                <li class="CartItem">
                    <div class="Container">
                        <div class="CartImage">
                            <?php echo "<img width='90%' src='img/" . $product->getImage() . "' />" ?>
                        </div>
                    </div>
                    <div class="Container">
                        <div class="CartName">
                            <p><?php echo $product->getName() ?></p>
                        </div>
                    </div>
                </li>
            <?php }
            echo "<h5> Order total: $" . $_SESSION["TC"] . "</h5>";
            ?>
        </div>
        <a href="Home.php">Continue Shopping</a>
    </div>
</div>
<?php
include $ini["Template"]["foot"];
?>
</body>
</html>