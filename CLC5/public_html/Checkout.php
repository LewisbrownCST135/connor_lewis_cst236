<?php
/*
Project: CST-236 CLC4 4.3
File: Checkout.php 1.0
Authors: Connor Low
Date: 10/22/17
Synopsis: Checkout products
*/

$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["header"];
require_once $ini["Path"]["secure"];
$totalCost = 0;
?>

<!DOCTYPE HTML>
<?php
$title = "Checkout - Retro Gamer";
include $ini["Template"]["head"];
?>

<body>
<?php include $ini["Template"]["nav"]; ?>
<div id="main_content">
    <div class="CenterContent">
        <div class="Title">
            <h3>Checkout for <?php echo $user->getFirstName() ?></h3>
        </div>
        <div class="CartContent">
            <?php include $ini["Tool"]["checkout"] ?>
        </div>
        <div class="CheckoutStep">
            <a href="CartView.php">Back to Cart</a></div>
        <?php
        if ($cartService->getProducts())//isset($cart) && $cart->getItemCount() > 0)
            echo "|<div class=\"CheckoutStep\"><a href=\"PlaceOrder.php\">Place Order</a></div>" ?>
    </div>
</div>
<?php
include $ini["Template"]["foot"];
?>
</body>
</html>