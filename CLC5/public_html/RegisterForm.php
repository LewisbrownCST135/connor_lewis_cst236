<?php
/*
Project: CST-236 CLC4 5.0
File: RegisterForm 2.0
Authors: Connor Low
Date: 10/26/17
Synopsis: Register a new user
*/
$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["autoloader"];
?>
<!DOCTYPE html>
<html lang="en">
<?php
$title = "Retro Gamer";
include $ini["Template"]["head"];
?>
<body>
<div id="main_content">
    <div id="main_nav">
        <a href="index.html">Retro Gamer</a>
    </div>
    <div class="CenterContent">
        <div class='Errors'>
            <div class="Container">
                <?php
                //<!--isset checks is a variable is set and is not null. it also returns true or false-->
                //$error is from register handler
                if (isset($error)) {
                    //if the error = 1 then
                    if ($error == 1)
                        echo "<p>Please fill out all fields according to directions.</p>";
                    if ($error == 2)
                        echo "<p>Username already taken!</p>";
                }
                ?>
            </div>
        </div>
        <div class="FormContainer">
            <form action="masterHandler.php?register" method="Post">
                <div class="SubForm">
                    <div class="Instructions">
                        <p>Username must be filled out</p>
                    </div>
                    <input name="username" placeholder="Username" maxlength="30"

                        <?php if (isset($user) && $user != NULL) echo("value='" . $user->getUsername() . "'"); ?>
                    ><br/>
                    <div class="Instructions">
                        <p>Password must be at least 4 charactes.</p>
                    </div>
                    <input type="password" name="password" placeholder="Password" maxlength="50"
                        <?php if (isset($user) && $user != NULL) echo("value='" . $user->getPassword() . "'"); ?>
                    ><br/>
                </div>
                <div class="SubForm">
                    <div class="Instructions">
                        <p>Provide a first and last name</p>
                    </div>
                    <!--if the user variable exist print the firstname of the user-->
                    <input name="firstName" placeholder="First" maxlength="30"
                        <?php if (isset($user) && $user != NULL) echo("value='" . $user->getFirstName() . "'"); ?>
                    ><br/>
                    <!--if the user variable exist print the lastname of the user-->
                    <input name="lastName" placeholder="Last" maxlength="30"
                        <?php if (isset($user) && $user != NULL) echo("value='" . $user->getLastName() . "'"); ?>
                    ><br/>
                    <div class="Instructions">
                        <p>Provide an email</p>
                    </div>
                    <!--if the user variable exist print the lastname of the user-->
                    <input name="email" placeholder="Email" maxlength="50"
                        <?php if (isset($user) && $user != NULL) echo("value='" . $user->getEmail() . "'"); ?>
                    ><br/>
                </div>
                <div class="SubForm">
                    <div class="Instructions">
                        <p>Provide Name listed on Card</p>
                    </div>
                    <div class="CardHolder">
                        <input title="Provide the name on card" name="holder" placeholder="John L. Smith"
                            <?php if (isset($user) && $user != NULL) echo("value='" . $user->getCardH() . "'"); ?>
                        >
                    </div>
                    <div class="Instructions">
                        <p>Provide card information</p>
                    </div>
                    <div class="Card">
                        <input name="cc1" placeholder="****" class="CardBit" value="0000" maxlength="4">
                        <input name="cc2" placeholder="****" class="CardBit" value="1111" maxlength="4">
                        <input name="cc3" placeholder="****" class="CardBit" value="2222" maxlength="4">
                        <input name="cc4" placeholder="****" class="CardBit" value="3333" maxlength="4"><br/>
                    </div>
                    <div class="CVV">
                        <input name="cvc" placeholder="CVC" class="CardBit"
                               maxlength="3"
                            <?php if (isset($user) && $user != NULL) echo("value='" . $user->getCardCVC() . "'"); ?>
                        ><br/>
                    </div>
                    <div class="Instructions">
                        <p>Fill out in format: yyyy/dd/mm</p>
                    </div>
                    <input name="expiration" placeholder="yyyy/dd/mm" maxlength="10"
                        <?php if (isset($user) && $user != NULL) echo("value='" . $user->getCardED() . "'"); ?>
                    >
                </div>
                <div class="SubForm">
                    <input class="Button" type="submit" value="Register">
                </div>
            </form>
        </div>
        <div class="PostForm">
            <div class="Container">
                <a href="LoginForm.php">Already have an account? Log in here.</a>
            </div>
        </div>
    </div>
</div>
<?php
include $ini["Template"]["foot"];
?>
</body>
</html>