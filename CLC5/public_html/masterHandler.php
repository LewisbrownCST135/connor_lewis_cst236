<?php
/*
Project: CST-236 CLC4 5.0
File: masterHandler 4.0
Authors: Connor Low
Date: 10/26/17
Synopsis: used to keep url in parent directory while utilizing the handlers in the func/ directory.
*/
$ini = parse_ini_file("../resource/config.ini", TRUE);

// Case switch
//Query_String is all the stuff in the url after the ?
switch ($_SERVER["QUERY_STRING"]) {
    case 'login' :
        include $ini["Path"]["handler"] . "loginHandler.php";
        break;
    case 'register':
        include $ini["Path"]["handler"] . "registerHandler.php";
        break;
    case 'user_search':
        include $ini["Path"]["handler"] . "searchHandler.php";
        break;
    case 'admin_findProduct':
        include $ini["Path"]["handler"] . "adminFindProductHandler.php";
        break;
    case 'admin_updateProduct':
        include $ini["Path"]["handler"] . "adminUpdateProductHandler.php";
        break;
    case 'admin_deleteProduct':
        include $ini["Path"]["handler"] . "adminDeleteProductHandler.php";
        break;
    case 'admin_addProduct':
        include $ini["Path"]["handler"] . "adminAddProductHandler.php";
        break;
    case 'bulkAddProduct':
        include $ini["Path"]["handler"] . "bulkHandler.php";
        break;
    case 'placeOrder':
        include $ini["Path"]["handler"] . "orderHandler.php";
        break;
    default :
        include $ini["Path"]["handler"] . "logout.php";
}