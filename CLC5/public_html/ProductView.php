<?php
/*
Project: CLC6 6.1
File: Edit Product Tool 2.0
Authors: Connor Low
Date: 12/8/17
Synopsis: View a product's information
References:
*/

$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["header"];
require_once $ini["Path"]["secure"];

$productService = new ProductBusinessService(new Product($_SERVER["QUERY_STRING"]));
$row = $productService->selectProduct()->fetch_assoc();
$product = new Product($row["ID"], $row["NAME"], $row["PRICE"], $row["DESCRIPTION"], $row["IMAGE"]);

?>
<!DOCTYPE HTML>
<html lang="en">
<?php
$title = $row["NAME"] . " - Retro Gamer";
include $ini["Template"]["head"];
?>

<body>
<?php include $ini["Template"]["nav"]; ?>

<div id="main_content">
    <div class="ToolNavigation">
        <a href="Home.php">Back to Home</a>
    </div>
    <div class="Tool">
        <?php if (isset($product) && $product->getId() != NULL) { ?>
        <div class="ProductViews">
            <div class="titleName">
                <h3>View Product: <?php echo $row["NAME"] ?></h3>
            </div>
            <div class="Instruction">
            </div>
            <div class="SplitSection">
                <section>
                    <div class="ProductForm">

                        <div class="ProductView">
                            <div class="img">

                                <h3>Image:</h3>
                                <img width="100px" src= <?php echo "\"img/" . $row["IMAGE"] . "\"" ?>>

                            </div>
                            <div class="Descript">
                                <h3>Properties:</h3>
                                <p><?php echo $row["DESCRIPTION"]; ?></p>
                                <p>$<?php echo $row["PRICE"]; ?></p>
                            </div>
                        </div>

                        <a <?php echo "href='CartView.php?action=add&productId=" . $_SERVER["QUERY_STRING"] . "'" ?>>Add
                            to
                            cart</a>
                        <?php
                        } else {
                            ?>
                            <h3>Error: no product selected.</h3>
                        <?php } ?>
                    </div>

                </section>

                <div class="SideCart">
                    <div class="Container">
                        <h3 class="CartHeader"><?php echo $_SESSION["user"]->getFirstName() ?>'s Cart</h3>
                        <?php include $ini["Tool"]["cart"]; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<?php include $ini["Template"]["foot"]; ?>
</html>
