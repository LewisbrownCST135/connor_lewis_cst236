<?php
/*
Project: CST-236 CLC4 3.2
File: home 2.1
Authors: Connor Low
Date: 10/8/17
Synopsis: secure home page
*/

$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["header"];
require_once $ini["Path"]["secure"];
?>
<!DOCTYPE HTML>
<html lang="en">
<?php
$title = "Retro Gamer";
include $ini["Template"]["head"];
?>

<body>
<?php include $ini["Template"]["nav"]; ?>
<div id="main_content">
    <div class="CenterContent">
        <div class="Title">
            <h1>Welcome <?php echo $user->getFirstName() ?>.</h1>
        </div>
        <div class="SplitSection">
            <section>
                <div class="ProductList">
                    <?php include $ini["Tool"]["catalog"] ?>
                </div>
            </section>
            <aside>
                <div class="SideCart">
                    <div class="Container">
                        <h3 class="CartHeader"><?php echo $user->getFirstName() ?>'s Cart</h3>
                        <?php include $ini["Tool"]["cart"]; ?>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</div>
<?php
include $ini["Template"]["foot"];
?>
</body>
</html>
