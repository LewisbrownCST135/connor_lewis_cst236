<?php
/*
Project: CST-236 CLC4 3.1
File: Admin Tools 1.1 (previously admin.php)
Authors: Connor Low
Date: 10/7/17
Synopsis: Loads all the ADMIN tools for managing products (Client end CRUD)
reference: func/connect.php
*/

$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["header"];
require_once $ini["Path"]["secure"];
?>
<!DOCTYPE HTML>
<html lang="en">
<?php
$title = "Admin - Retro Gamer";
include $ini["Template"]["head"];
?>

<body>
<?php include $ini["Template"]["nav"]; ?>
<div id="main_content">
    <div class="CenterContent">
        <div class="Container">
            <div class="ToolBar">
                <a href="AdminTools.php?action=add">Add New Product</a>
                <a href="AdminTools.php?action=addAll">Bulk Add Products</a>
                <a href="AdminTools.php?action=find">Find a Product</a>
                <a href="AdminTools.php?action=report&page=0">Business Report</a>
            </div>
        </div>

        <div id="Tool">
            <div class="Container">
                <?php
                if (isset($_SERVER["QUERY_STRING"]) && trim($_SERVER["QUERY_STRING"]) != "") {
                    switch ($_GET["action"]) {
                        case "add" :
                            include($ini["Tool"]["add"]);
                            break;
                        case "addAll" :
                            include($ini["Tool"]["bulk"]);
                            break;
                        case "find" :
                            include($ini["Tool"]["find"]);
                            break;
                        case "delete" :
                            include($ini["Tool"]["delete"]);
                            break;
                        case "report" :
                            include ($ini["Tool"]["report"]);
                            break;
                        case "info" :
                            include ($ini["Tool"]["info"]);
                            break;
                        default :
                            if ((int)$_GET["id"] > 0)
                                include($ini["Tool"]["edit"]);
                            else
                                echo "No tool selected";
                    }
                } else {
                    echo "No tool selected.";
                }

                ?>
            </div>
        </div>
    </div>
</div>
<?php include $ini["Template"]["foot"]; ?>
</body>
</html>
