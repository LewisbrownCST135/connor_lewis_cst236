<?php

class Formatter
{
    private $input;
    private $output;

    /**
     * Beautify constructor.
     * @param $input
     */
    public function __construct($input)
    {
        $this->input = $input;
        $this->output = "";
    }

    /**
     * @return mixed
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @param mixed $input
     */
    public function setInput($input)
    {
        $this->input = $input;
    }

    /**
     * @return string
     */
    public function getOutput()
    {
        return $this->output;
    }


    function json()
    {
        $this->output = "";
        $indent = 0;
        $i = 0;
        while ($i < strlen($this->input)) {
            $c = substr($this->input, $i, 1);
            if ($c == "\"") {
                $this->output .= $c;
                do {
                    $c = substr($this->input, ++$i, 1);
                    $this->output .= $c;
                } while ($c != "\"");
            } elseif ($c == "{" || $c == "[") {
                $this->output .= $c . "\n";
                $indent++;
                for ($tab = 0; $tab < $indent; $tab++) {
                    $this->output .= "\t";
                }
            } elseif ($c == "}" || $c == "]") {
                $this->output .= "\n";
                $indent--;
                for ($tab = 0; $tab < $indent; $tab++) {
                    $this->output .= "\t";
                }
                $this->output .= $c;
            } elseif ($c == "," || $c == ";") {
                $this->output .= $c . "\n";
                for ($tab = 0; $tab < $indent; $tab++) {
                    $this->output .= "\t";
                }
            } else
                $this->output .= $c;
            $i++;
        }
        return $this->output;
    }
}