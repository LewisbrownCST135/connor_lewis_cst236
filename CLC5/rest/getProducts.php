<?php
header('Content-Type: text/plain');
$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["autoloader"];
require_once "Formatter.php";

$name = $_GET["name"];
$min = $_GET["min_price"];
$max = $_GET["max_price"];

$service = new ProductBusinessService();
$products = $service->restGet($name, $min, $max);

$json = json_encode($products);
$formatter = new Formatter($json);

echo $formatter->json();