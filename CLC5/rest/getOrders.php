<?php
header('Content-Type: text/plain');
$ini = parse_ini_file("../resource/config.ini", TRUE);
require_once $ini["Path"]["autoloader"];
require_once "Formatter.php";

$user = $_GET["user"];
$product = $_GET["product"];

$service = new OrderBusinessService();
$orders = $service->restGet($user, $product);

$json = json_encode($orders);
$formatter = new Formatter($json);

echo $formatter->json();